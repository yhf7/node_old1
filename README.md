# node

#### 项目介绍

yhf-vue 项目后台

#### 软件架构

app 主骨架
router 路由
controller 业务逻辑
model 操作数据库
VueUsers.sql 数据库骨架

#### 安装教程

1. 直接文件下载
2. git 下载 sudo git clone https://gitee.com/yhf7/node.git

#### 使用说明

1. 进入文件
2. 运行 sudo npm i 安装依赖 （npm 或 cnpm）
3. 运行 sudo node app.js 开启服务 （node 或 nodemon）
4. 开启数据库 把 VueUsers.sql 文件导入

#### Github

[欢迎来我的 Github](https://github.com/YHF7)
